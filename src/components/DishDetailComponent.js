import React from 'react';
import {Card, CardImg, CardBody, CardTitle, CardText} from 'reactstrap';
import moment from 'moment';

function RenderDish({dish}) {
    return (
        <Card>
            <CardImg top src={dish.image} alt={dish.name}/>
            <CardBody>
                <CardTitle>{dish.name}</CardTitle>
                <CardText>{dish.description}</CardText>
            </CardBody>
        </Card>
    )
};

function RenderComments({comments}) {
    const commentsBody = Object
        .keys(comments)
        .map((i) => {
            const comment = comments[i];
            return (
                <div key={comment.id}>
                    <div>{comment.comment}</div>
                    <div>
                        --{comment.author}, {moment(comment.date).format('ll')}
                    </div>
                </div>
            );
        });

    if (comments) {
        return (
            <div>
                <h4>Comments</h4>
                <ul className="list-unstyled">
                    {commentsBody}
                </ul>
            </div>
        );
    } else {
        return <div></div>;
    }
};

const DishDetail = (props)=>{    
    if (props.dish) {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-12 col-md-5 m-1">
                        <RenderDish dish={props.dish}/>
                    </div>
                    <div className="col-12 col-md-5 m-1">
                        <RenderComments comments={props.dish.comments}/>
                    </div>
                    <br/>
                </div>
            </div>
        );
    } else {
        return <div></div>;
    }
}

export default DishDetail;